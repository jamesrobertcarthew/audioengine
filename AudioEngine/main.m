//
//  main.m
//  AudioEngine
//
//  Created by James Carthew on 9/12/2014.
//  Copyright (c) 2014 James Carthew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
