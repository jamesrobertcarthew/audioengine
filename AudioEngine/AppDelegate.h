//
//  AppDelegate.h
//  AudioEngine
//
//  Created by James Carthew on 9/12/2014.
//  Copyright (c) 2014 James Carthew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

